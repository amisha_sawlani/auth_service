package com.auth.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


//when try-catch blocks are unable to catch exceptions, it will call "MyControllerAdvice" to handle exceptions which
//will call EmptyInputException class
@ControllerAdvice
public class MyControllerAdvice {
    @ExceptionHandler(EmptyInputException.class)
    public ResponseEntity<String> handleEmptyInput(EmptyInputException emptyInputException)
    {
        return new ResponseEntity<String>("Input Field is empty or 0", HttpStatus.BAD_REQUEST);
    }
}
