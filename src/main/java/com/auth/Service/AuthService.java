package com.auth.Service;

import java.sql.Timestamp;

public interface AuthService {

    public int generateOTP();  //generates OTP for a new user or existing user with new device

	public boolean verifyOtp(int generatedOtp, int UserGivenOtp);  //verifies generated OTP with user given OTP

	public String generateAuthToken(); //generates Auth token while login

	public int checkUserExists(String UserDeviceDetails); //check if user exists with a particular mobile number, device_id and device model
	
	public int verifyTokenExpiry(String UserDeviceDetails); //verifies if the token is expired or not for given user
	
	public int validateToken(String AuthToken,String deviceId); //validates for a particular exisiting user if he logged in within 2 hours
	
	public String saveUser(String UserDeviceDetails,long UserId,String AuthToken,String deviceId);  //saves user details(userdevicedetails,userid,authtoken,device id) into database
	
//	public int generateDeviceID();
	
	public String getValidAuthToken(String UserDeviceDetails);  //returns auth token for a given existing user
	
	public Timestamp getCurrentTime();  //returns current time
	
	public Timestamp getOtpExpiryTime(); //sets user OTP expiry time to 10 minutes
	
	public int checkOtpExpiry(Timestamp OtpExpiry);  //checks whether user enters OTP within given OTP expiry time
	
	public int getByUserId(String userDeviceDetails); //retrieves user id for a user with given user-device-details
	
	public boolean ChecknumberValidity(String number,String device_model,String device_id); //ensures that phone number,device model and device id are not empty
	       //also ensures that the phone number is of 10 digits and in the given format

	void storeGeneratedOTP(int generatedOtp);  //stores generated OTP

	int getStoredGeneratedOTP(); //returns stored generated OTP to be compared with user entered  OTP in the OTP Controller
}
